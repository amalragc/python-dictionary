class BinaryTreeOps:
    def constructBST(keys):
        root = None
        for key in keys:
            root = BinaryTreeOps.insert(root, key)
        return root

    def insert(root, key):
        curr = root
        parent = None
        if root is None:
            return Node(key)
        while curr:
            parent = curr
            if key < curr.data:
                curr = curr.left
            else:
                curr = curr.right
        if key < parent.data:
            parent.left = Node(key)
        else:
            parent.right = Node(key)
        return root

    def search(root, key):
        while root != None:
            prompt = root.data.split("/")
            word = prompt[0]
            # if key > root.data:
            if key > word:
                root = root.right
            # elif key < root.data:
            elif key < word:
                root = root.left
            else:
                meaning = root.data.split("/")
                meaning = meaning[1]
                result_word_and_meaning = "{} - {}".format(key, meaning)
                return result_word_and_meaning
        result_not_found = "{} - not found".format(key)
        return result_not_found

    def search_substring(value: str):
        words_containing_substring = []
        for i in words_in_dictionary:
            if i.__contains__(value):
                words_containing_substring.append(i)
        return words_containing_substring

    def inorder(root):
        if root is None:
            return
        BinaryTreeOps.inorder(root.left)
        prompt = root.data.split("/")
        word = prompt[0]
        words_in_dictionary.append(word)
        BinaryTreeOps.inorder(root.right)


# A class to store a BST node
class Node:
    # Function to create a new binary tree node having a given key
    def __init__(self, key):
        self.data = key
        self.left = self.right = None


class FileOps:
    def read_from_file(filepath):
        file_contents = None
        with open(filepath) as file:
            file_contents = file.readlines()
            file.close()
            return file_contents

    def write_to_file(filename, data):
        with open(filename, 'a') as file:
            for line in data:
                file.write(line)
            file.close()

    def process_search_prompts_file(file_contents, root):
        for i in file_contents:
            if(i.__contains__("SearchWord")):
                word = FileOps.isolate_search_word(i)
                r = BinaryTreeOps.search(root, word)
                search_word_results.append(r)
            if(i.__contains__("SubString:")):
                word = FileOps.isolate_search_word(i)
                substring_prompts.append(word)

    def isolate_search_word(search_input):
        search_input = search_input.replace(" ", "")
        prompt = search_input.split(":")
        word = prompt[1].replace(" ", "")
        word = prompt[1].replace("\n", "")
        return word


def init():
    insert_words_file = "./insert_file.txt"
    search_prompts_file = "./search_file.txt"
    output_file = "./output_file.txt"
    input_words = FileOps.read_from_file(insert_words_file)
    search_words = FileOps.read_from_file(search_prompts_file)
    FileOps.write_to_file(
        output_file, "-----------Reading from file {} -------------\n".format(insert_words_file))
    FileOps.write_to_file(
        output_file, "BST Created with {} nodes \n".format(len(input_words)))
    FileOps.write_to_file(
        output_file, "---------------------------------------------------\n---------------- Search words ------------------------\n")
    for i in range(len(input_words)):
        input_words[i] = input_words[i].replace(" / ", "/")
        input_words[i] = input_words[i].replace("\n", "")
    root = BinaryTreeOps.constructBST(input_words)
    BinaryTreeOps.inorder(root)
    FileOps.process_search_prompts_file(search_words, root)
    for i in range(len(search_word_results)):
        FileOps.write_to_file(
            output_file, "{}\n".format(search_word_results[i]))
    FileOps.write_to_file(output_file, ".................\n")
    for i in range(len(substring_prompts)):
        r = BinaryTreeOps.search_substring(substring_prompts[i])
        substring_words_for_writing_in_file = ""
        for j in range(len(r)):
            substring_words_for_writing_in_file += "{},".format(r[j])
        substring_words_for_writing_in_file = substring_words_for_writing_in_file[:-1]
        FileOps.write_to_file(
            output_file, "---------------- Sub String: {} ------------------------\n".format(substring_prompts[i]))
        FileOps.write_to_file(output_file, "{}\n".format(
            substring_words_for_writing_in_file))
    FileOps.write_to_file(output_file, ".................\n")


global words_in_dictionary
global search_word_results
global substring_prompts
global search_substring_results
search_substring_results = []
substring_prompts = []
words_in_dictionary = []
search_word_results = []

init()
